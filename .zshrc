#
# Executes commands at the start of an interactive session.
#
# Authors:
#   Sorin Ionescu <sorin.ionescu@gmail.com>
#

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

# Customize to your needs...

function iterm2_print_user_vars() {
  iterm2_set_user_var kubens $(kubectl config view -o=jsonpath="{.contexts[?(@.name==\"$(kubectl config current-context)\")].context.namespace}")
  iterm2_set_user_var kubecontext $(kubectl config current-context)
}

test -e "${HOME}/.iterm2_shell_integration.zsh" && source "${HOME}/.iterm2_shell_integration.zsh"

# Added by Krypton
export GPG_TTY=$(tty)
export SSLKEYLOGFILE=~/.ssl-key.log

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
