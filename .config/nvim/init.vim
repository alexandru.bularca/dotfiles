if &compatible
  set nocompatible               " Be iMproved
endif

" install vim plug
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

runtime core.vim          " core settings for how vim functions
runtime plugins.vim       " use Plug to install and load the user specific plugins
runtime settings.vim      " user specific settings (color scheme, preferred tab sizing) go here
runtime mappings.vim      " command mappings go here
