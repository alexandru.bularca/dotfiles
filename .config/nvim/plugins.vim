call plug#begin(stdpath('data') . '/plugged')
  " utilities
  Plug 'junegunn/seoul256.vim'
  Plug 'connorholyday/vim-snazzy'
  Plug 'mhartington/oceanic-next'
  Plug 'vim-airline/vim-airline'
  Plug 'editorconfig/editorconfig-vim'
  Plug 'jiangmiao/auto-pairs'
  Plug 'tpope/vim-commentary'
  Plug 'tpope/vim-surround'
  Plug 'tpope/vim-obsession'
  Plug 'tpope/vim-fugitive'
  Plug 'christoomey/vim-tmux-navigator'
  Plug 'udalov/kotlin-vim'

  " syntax highlight
  Plug 'evanleck/vim-svelte', {'branch': 'main'}

  " file navigation
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
  Plug 'junegunn/fzf.vim'
  Plug 'HerringtonDarkholme/yats.vim'

  " completion
  Plug 'neoclide/coc.nvim', {'branch': 'release'}

  " Completion plugins
  Plug 'iamcco/coc-actions', {'do': 'npm install'}
  Plug 'neoclide/coc-json', {'do': 'npm install'}
  Plug 'neoclide/coc-highlight', {'do': 'npm install'}
  Plug 'neoclide/coc-snippets', {'do': 'npm install'}
  Plug 'neoclide/coc-yaml', {'do': 'npm install'}
  Plug 'voldikss/coc-template', {'do': 'npm install'}

  " Language servers
  Plug 'josa42/coc-go', {'do': 'npm install'}
  Plug 'clangd/coc-clangd', {'do': 'npm install'}
  Plug 'coc-extensions/coc-svelte', {'do': 'npm install'}
  Plug 'pappasam/coc-jedi', {'do': 'npm install'}
  Plug 'amiralies/coc-elixir', {'do': 'npm install'}
  Plug 'neoclide/coc-java', {'do': 'npm install'}
  Plug 'neoclide/coc-rls', {'do': 'npm install'}
  Plug 'neoclide/coc-tsserver', {'do': 'npm install'}
  Plug 'neoclide/coc-vetur', {'do': 'npm install'}
  Plug 'iamcco/coc-vimlsp', {'do': 'npm install'}

  " web dev
  Plug 'iamcco/coc-angular', {'do': 'npm install'}
  Plug 'neoclide/coc-css', {'do': 'npm install'}
  Plug 'neoclide/coc-emmet', {'do': 'npm install'}
  Plug 'neoclide/coc-html', {'do': 'npm install'}
call plug#end()

" --------------------- [Plugin Settings] ------------------------
" [FzF]
let g:fzf_nvim_statusline = 0 "
